#ifndef _TESTING_
#define _TESTING_ 1

#include <iostream>
#include "database.h"
#include "orders.h"
#include "elevatorCar.h"
#include "product.h"
#include "stores.h"

using namespace std;

/** 
 * It shows the data required by the programmer for testing the code and its proper use.
 * @param ppStoresCrisFran ElevatorCar which represents an array of carts.
 * @param ppOrderList OrderTail A representing an array of work order queues.
 * @param ppPickCars Store which represents an array of warehouses.
*/
void consulting(Store ppStoresCrisFran[], OrderTail ppOrderList[], ElevatorCar ppPickCars[]) {
    /** Consulting  Stores *
    cout<<"\n-----Fruits-----\n";
    pStoresCrisFran[0].listOfProducts();
    pStoresCrisFran[0].productOne->columsDetails();
    pStoresCrisFran[0].productOne->firstColum->palletsDetails();
    /**cout<<"\n-----Meat-----\n";
    pStoresCrisFran[1].listOfProducts();
    cout<<"\n-----Icecreams-----\n";
    pStoresCrisFran[2].listOfProducts();
    cout<<"\n-----Drinks-----\n";
    pStoresCrisFran[3].listOfProducts();
    cout<<"\n-----Electronics-----\n";
    pStoresCrisFran[4].listOfProducts();
    cout<<"Code: "<<pStoresCrisFran[4].storeCode<<endl;
    
    /** Consulting  Orders *
    cout<<"\n-----Order-----\n";
    pOrderList[0].listOrderProducts();
    cout<<"\n-----Order-----\n";
    pOrderList[1].listOrderProducts();
    cout<<"\n-----Order-----\n";
    pOrderList[2].listOrderProducts();
    cout<<"\n-----Order-----\n";
    pOrderList[3].listOrderProducts();
    cout<<"\n-----Order-----\n";
    pOrderList[4].listOrderProducts();
    

    /** Consulting Order Tails By pPickCars *
    cout<<"\n-----Pick Car number 1-----" <<endl;
    pPickCars[0].orderTail->listOrderProducts();
    cout<<"\n-----Pick Car number 2-----" <<endl;
    pPickCars[1].orderTail->listOrderProducts();
    cout<<"\n-----Pick Car number 3-----" <<endl;
    pPickCars[2].orderTail->listOrderProducts();
    cout<<"\n-----Pick Car number 4-----" <<endl;
    pPickCars[3].orderTail->listOrderProducts();
    cout<<"\n-----Pick Car number 5-----" <<endl;
    pPickCars[4].orderTail->listOrderProducts();


    /** Pop some products *
    pStoresCrisFran[0].popProduct();
    pPickCars[4].orderTail->popOrder();*/

    /** Re-Consulting *
    cout<<"\n-----Fruits-----\n";
    pStoresCrisFran[0].listOfProducts();
    cout<<"\n-----Electronics-----\n";
    pStoresCrisFran[4].listOfProducts();
    cout<<"Code: "<<pStoresCrisFran[4].storeCode<<endl;
    cout<<"\n-----Pick Car number 5-----" <<endl;
    pPickCars[4].orderTail->listOrderProducts();*/
    
}

#endif //_TESTING_
#ifndef _WORKPROCESS_
#define _WORKPROCESS_ 1
#define AVAIABLES 5
#define INDEX 5

#include "elevatorCar.h"
#include "stores.h"

void getDownThings(Store *pStore, ElevatorCar &pPickCar);
void printRecipe(ElevatorCar *pPickCar);
void popAllOrder(ElevatorCar *pPickCar);
int getTailIndex(ElevatorCar &pPickCar);

/**
 * It goes through the order queues of each cart and takes out the requested products from the warehouses.
 * @param pStoreList Represents the Array of warehouses.
 * @param Represents the Array of carts.
*/
void doWorkProcess(Store pStoreList[], ElevatorCar &pPickCar) {
    
    // For a list of warehouses.
    for (int storeIndex = 0; storeIndex < INDEX; storeIndex++) {
        // Validate the warehouses.
        if (storeIndex != pPickCar.orderTail->storeCode) {
            continue;
        }
        else {
            // Selec one warehouse.
            Store *tempStore = &pStoreList[storeIndex];
            getDownThings(tempStore, pPickCar);
        }
    }

    // Print Recipe
    printRecipe(&pPickCar);
}

/** 
 * It does the work of looking for the products in the columns and pushing and popping the pallets respectively.
 * @param pStore Store that represents the winery in which it will work.
 * @param pPickCar ElevatorCar representing the cart in charge of the job.
*/
void getDownThings(Store *pStore, ElevatorCar &pPickCar) {
    // Drives in the Orders
    for (OrderProduct *tempOrder = pPickCar.orderTail->start; tempOrder != NULL; tempOrder = tempOrder->next) {
        // Drives in the Stores
        for (Product *tempProduct = pStore->productOne; tempProduct != NULL; tempProduct = tempProduct->next) {
            // Both equals
            if (tempProduct->getNameProduct().compare(tempOrder->getNameProduct()) == 0) {
                int unitsRequested = tempOrder->unitsInTotal;
                if (tempProduct->unitsInTotal >= unitsRequested) {
                    // Drives in colums.
                    for(Colums *tempColum = tempProduct->firstColum; tempColum != NULL; tempColum = tempColum->next) {
                        if(tempColum->firstPallet->units <= unitsRequested) {
                        unitsRequested -= tempColum->firstPallet->getUnits();
                        tempColum->unitsPerColums -= unitsRequested;
                        tempColum->popPallet();
                        pPickCar.addTime(pPickCar.getActionTime() * 1);
                        }
                        if (tempColum->firstPallet->units > unitsRequested) {
                            int tempUnits = tempColum->firstPallet->getUnits() - unitsRequested;
                            tempColum->popPallet();
                            tempColum->pushPallet(tempUnits);
                            pPickCar.addTime(pPickCar.getActionTime() * 2);
                            unitsRequested -= unitsRequested;
                            tempColum->unitsPerColums -= unitsRequested;
                        }
                        if (tempColum->unitsPerColums > 0) {
                            tempColum = tempProduct->firstColum;
                        }
                        if (unitsRequested == 0) {
                            break;
                        }
                    }
                    tempOrder->isCompleted = 'V';
                    break;
                }
            }
            else {
                continue;
            }
        }
    }
}

/** 
 * Show the receipt with the status of the work order and its respective time.
 * @param pPickCar ElevatorCar representing the respective cart.
*/
void printRecipe(ElevatorCar *pPickCar) {
    /** Recipe */
    cout<<"\n-------------- START OF RECIPE --------------";
    pPickCar->orderTail->listOrderProducts();
    cout<<"\nTotal time in milisecods working in the order: " <<pPickCar->getTotalTimeOfWork();
    cout<<"\n-------------- END OF RECIPE ---------------\n";

    /** Cleaning */
    popAllOrder(pPickCar);
}

/** 
 * Clear the job order queue.
 * @param pPickCar ElevatorCar represents the car to clean the respective work order.
*/
void popAllOrder(ElevatorCar *pPickCar) {
    // Cleaning up the order tail.
    while (pPickCar->orderTail->start != NULL) {
        pPickCar->orderTail->popOrder();
    } 
}

/** 
 * Gets the number of nodes in a queue.
 * @param pPickCar ElevatorCar represents the cart with the queue to find out.
 * @return index Int A representing the number of items in the pPickCar queue.
*/
int getTailIndex(ElevatorCar &pPickCar) {
    int index;
    for (OrderProduct *tempOrder = pPickCar.orderTail->start; tempOrder != NULL; tempOrder = tempOrder->next) {
        index++;
    }
    return index;
};

#endif //_WORKPROCESS_
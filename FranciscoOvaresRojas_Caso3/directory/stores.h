#ifndef _STORE_
#define _STORE_ 1

#include "product.h"

static int contStores = 1;

struct Store {
    /** Atributes */
    Product *productOne = NULL;
    int storeCode;

    /** 
     * Builder of the struct.
    */
    Store() {
        this->storeCode = contStores;
        contStores++;
    }

    /** 
     * Creates a product node and adds it to the stack.
     * @param pName string that represents the name of the product.
     * @param pColums int that represents the number of colums per product.
     * @param pPalletsPerColum int which represents the number of pallets that the column will have.
     * @param pUnitsPerPallet int A representing the number of units per pallet.
    */
    void pushProduct(string pName, int pColums, int pPalletsPerColum, int pUnitsPerPallet) {
        // Crafting Product
        Product *newProduct = new Product(pName, pColums, pPalletsPerColum, pUnitsPerPallet);
        // Pusshing product
        newProduct->next = productOne;
        productOne = newProduct;
    }
    /** 
     * Removes the top product from the stack.
    */
    void popProduct() {
        Product *auxProduct = productOne;
        productOne = auxProduct->next;
        delete auxProduct;
    }

    /** 
     * Displays the values of the product attributes.
    */
    void listOfProducts(){    
        if (productOne != NULL){
            for (Product *temp = productOne; temp != NULL; temp=temp->next) {
                cout<<"\nName: " <<temp->getNameProduct() <<endl <<"Colums of Product:" <<temp->getColums() <<endl;
                cout<<"Pallets per Column: " <<temp->getPalletsPerColum() <<endl;
                cout<<"Units per Pallet: " <<temp->getUnitsPerPallet() <<endl;
                cout<<"Units in Total: " <<temp->getUnitsInTotal() <<endl;

            }
        }
    }

};


#endif //_STORE_
#ifndef _DATABASE_
#define _DATABASE_ 1
#define QUANTITY 5

#include "stores.h"
#include "product.h"
#include "orders.h"
#include "elevatorCar.h"

/** 
 * Loads the burned data from the warehouses.
 * @param pStoreList Store that represents the array of warehouses.
*/
void uploadProducts(Store pStoreList[]) {
    // string pName, int pColums, int pPalletsPerColum, int pUnitsPerPallet
    
    /** Pushing Fruits*/
    pStoreList[0].pushProduct("Manzana", 3, 5, 20);
    pStoreList[0].pushProduct("Fresas", 3, 5, 40);
    pStoreList[0].pushProduct("Uvas", 3, 5, 50);

    /** Pushing Meat*/
    pStoreList[1].pushProduct("Pig", 3, 5, 10);
    pStoreList[1].pushProduct("Cow", 3, 5, 5);
    pStoreList[1].pushProduct("Chicken", 3, 5, 20);
    pStoreList[1].pushProduct("Fish", 3, 5, 25);

    /** Pushing Icecream*/
    pStoreList[2].pushProduct("Lemon Icecream", 3, 4, 10);
    pStoreList[2].pushProduct("Chocolate Icecream", 3, 4, 10);
    pStoreList[2].pushProduct("Strawberry Icecream", 3, 4, 5);
    
    /** Pushing Drinks*/
    pStoreList[3].pushProduct("Cacique", 3, 5, 50);
    pStoreList[3].pushProduct("Milk", 3, 5, 30);
    pStoreList[3].pushProduct("Apple Juice", 3, 5, 20);
    pStoreList[3].pushProduct("Yogurt", 3, 5, 25);

    /** Pushing Eletronics */
    pStoreList[4].pushProduct("Computer", 3, 3, 8);
    pStoreList[4].pushProduct("TV", 3, 4, 10);
    pStoreList[4].pushProduct("Phone", 3, 10, 30);
    pStoreList[4].pushProduct("Router", 3, 5, 20);
    pStoreList[4].pushProduct("Laptop", 3, 4, 10);
}

/** 
 * Load work orders with burned data.
 * @param pOrderList OrderTail array that represents the work orders to which the data will be loaded.
*/
void uploadOrders(OrderTail pOrderList[]) {
    // string pName, int pQuantity
    
    /** Pushing Orders */
    pOrderList[0].pushOrder("Fresas", 100, 0);
    pOrderList[0].pushOrder("Manzana", 150, 0);
    pOrderList[0].pushOrder("Uvas", 200, 0);

    pOrderList[1].pushOrder("Pig", 20, 1);
    pOrderList[1].pushOrder("Cow", 80, 1);
    pOrderList[1].pushOrder("Chicken", 70, 1);
    pOrderList[1].pushOrder("Fish", 20, 1);

    pOrderList[2].pushOrder("Lemon Icecream", 40, 2);
    pOrderList[2].pushOrder("Chocolate Icecream", 20, 2);
    pOrderList[2].pushOrder("Strawberry Icecream", 50, 2);

    pOrderList[3].pushOrder("Cacique", 40, 3);
    pOrderList[3].pushOrder("Milk", 15, 3);
    pOrderList[3].pushOrder("Apple Juice", 20, 3);
    pOrderList[3].pushOrder("Yogurt", 70, 3);

    pOrderList[4].pushOrder("Computer", 200, 4);
    pOrderList[4].pushOrder("TV", 15, 4);
    pOrderList[4].pushOrder("Phone", 100, 4);
    pOrderList[4].pushOrder("Laptop", 200, 4);  
}

/** 
 * Distribute work orders among existing carts.
 * @param pOrderList OrderTail which represents an array with the work orders to be distributed.
 * @param pPickCars ElevatorCar which represents an array of carts to which work orders will be assigned.
*/
void asignOrdersToPickCars(OrderTail pOrderList[], ElevatorCar pPickCars[]) {
    for (int index = 0; index < QUANTITY; index++){
        OrderTail *value = &pOrderList[index]; 
        /** Asigning the order to the PickCar */
        pPickCars[index].asginOrderTail(value);
    }
}

#endif //_DATABASE_
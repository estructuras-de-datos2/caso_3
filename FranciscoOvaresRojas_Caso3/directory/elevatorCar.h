#ifndef _ELEVATORCAR_
#define _ELEVATORCAR_ 1

#include "orders.h"
#include <stdlib.h>
#include <ctime>

struct ElevatorCar {
    /** Atributes */
    OrderTail *orderTail = NULL;
    int actionTime;
    int totalTimeWork = 0;

    /** 
     * Set an OrderTail to the respective struct ElevatorCar.
     * @param pOrdeTail Ordertail to be assigned to the respective ElevatorCar.
    */
    void asginOrderTail(OrderTail *pOrderTail) {
        this->orderTail = pOrderTail;
        srand(time(NULL));
        this->actionTime = 1 + rand() % 10;
    }

    /** get & set methods*/
    void addTime(int pTime){
        this->totalTimeWork += pTime;
    }
    int getActionTime() {
        return this->actionTime;
    }
    int getTotalTimeOfWork() {
        return this->totalTimeWork;
    }
    void setTotalTimeOFWork(int pTime) {
        this->totalTimeWork = pTime;
    }
};


#endif //_ELEVATORCAR_
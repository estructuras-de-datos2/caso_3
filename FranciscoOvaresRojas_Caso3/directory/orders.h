#ifndef _ORDERS_
#define _ORDERS_ 1

#include "stores.h"

struct OrderProduct {
    /** Atributes */
    string nameProduct;
    int unitsInTotal;
    char isCompleted = 'F';
    OrderProduct *next;

    /** 
     * Builder of the struct.
     * @param pName string that represents the name of the product solicited.
     * @param pQuantity int thta represents the quantity solicited of the product.
    */
    OrderProduct(string pName, int pQuantity) {
        this->nameProduct = pName;
        this->unitsInTotal = pQuantity;
    }

    /** set & get Methods */
    string getNameProduct() {
        return this->nameProduct;
    }
    int getQuantitySolicited() {
        return this->unitsInTotal;
    }
    string getIsCompleted() {
        if (this->isCompleted == 'V') {
            return "Yes.";
        }
        else{
            return "No.";
        }     
    }
};

struct OrderTail {
    /** Atributes */
    int storeCode;
    OrderProduct *start = NULL;
    OrderProduct *final = NULL;

    /** 
     * Crea una orden de trabajo y le hace push en la cola de ordenes enlazada.
     * @param pName string that represents the name of the product solicited.
     * @param pQuantity int that represents the quantity solicited of the product.
     * @param pStoreCode int that represents the code of the store.
    */
    void pushOrder(string pName, int pQuantity, int pStoreCode) {
        // Crafting
        OrderProduct *newProduct = new OrderProduct(pName, pQuantity);
        this->storeCode = pStoreCode;
        newProduct->next = NULL;
        // Pushing
        if (tailEmpty(start)) {
            this->start = newProduct;
        }
        else {
            this->final->next = newProduct;
        }
        this->final = newProduct;
    }

    /** 
     * Pop the first entered element of the queue.
    */
    void popOrder() {
        OrderProduct *auxProduct = start;
        if (start == final) {
            start = NULL;
            final = NULL;
        }
        else {
            start = start->next;
        }
        delete auxProduct;
    }

    /** 
     * Check if the queue is empty.
     * @param pOrder OrderProduct which represents the work order to be analyzed.
    */
    bool tailEmpty(OrderProduct *pOrder) {
        return (start == NULL)? true : false;
    }
    
    /** 
     * Displays the values of the OrderProduct attributes.
    */
    void listOrderProducts(){    
        if (start != NULL){
            for (OrderProduct *temp = start; temp != NULL; temp = temp->next) {
                cout<<"\nName: " <<temp->getNameProduct() <<endl;
                cout<<"Quantity Solicited: " <<temp->getQuantitySolicited() <<endl;
                cout<<"Is completed: " << temp->getIsCompleted() <<endl;
            }
        }
    }
};

#endif //_ORDERS_
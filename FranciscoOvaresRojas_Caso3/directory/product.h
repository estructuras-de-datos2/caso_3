#ifndef _PRODUCT_
#define _PRODUCT_ 1

#include <iostream>

using namespace std;

struct Pallets {
    /** Atributes */
    int units;
    Pallets *next = NULL;

    Pallets(int pUnits) {
        this->units = pUnits;
    }

    int getUnits() {
        return this->units;
    }
};

struct Colums {
    /** Atributes */
    int pallets;
    int units;
    int unitsPerColums;
    Colums *next = NULL;
    Pallets *firstPallet = NULL;

    /** 
     * Builder of the struct.
     * @param pPallets int which represents the number of pallets that the column will have.
     * @param pUnits int A representing the number of units per pallet.
    */
    Colums(int pPallets, int pUnits) {
        this->pallets = pPallets;
        this->units = pUnits;
        this->unitsPerColums = pUnits * pPallets;
        pushPallet(pUnits);
    }

    /** 
     * Creates pallets and stacks them based on the number of pallets per column of the product.
     * @param pUnits int that represents the number of units per pallet.
    */
    void pushPallet(int pUnits) {
        for (int index = 0; index < getPallets(); index++) {
            // Crafting colum
            Pallets *newPallet = new Pallets(pUnits);
            // Pusshing product
            newPallet->next = firstPallet;
            firstPallet = newPallet;
        }
    }
    /** 
     * Removes the first pallet from the pallet stack.
    */
    void popPallet() {
        Pallets *auxPallet = firstPallet;
        firstPallet = auxPallet->next;
        delete auxPallet;
    }

    /** set & get methods */
    int getUnitPerColum() {
        return this->unitsPerColums;
    }
    int getPallets() {
        return this->pallets;
    }
    int getUnitsPerPallet() {
        return this->units;
    }

    /** 
     * Displays the values of the attributes of the pallets.
    */
    void palletsDetails(){    
        if (firstPallet != NULL){
            int cont = 0;
            for (Pallets *tempPallet = firstPallet; tempPallet != NULL; tempPallet = tempPallet->next) {
                cout<<"----- Pallet number " <<++cont <<" -----" <<endl;
                cout<<"Units: " <<tempPallet->units <<endl;
            }
        }
    }
};
 
struct Product {
    /** Atributes */
    string nameProduct;
    int columsQuantity;
    int palletsPerColumn;
    int unitsPerPallet;
    int unitsInTotal;
    Colums *firstColum = NULL;
    Product *next = NULL;

    /** 
     * Builder of the strcut.
     * @param pName string that represents the name of the product.
     * @param pColums int that represents the number of colums per product.
     * @param pPalletsPerColum int which represents the number of pallets that the column will have.
     * @param pUnitsPerPallet int A representing the number of units per pallet.
    */
    Product(string pName, int pColums, int pPalletsPerColum, int pUnitsPerPallet) {
        this->nameProduct = pName;
        this->columsQuantity = pColums;
        this->palletsPerColumn = pPalletsPerColum;
        this->unitsPerPallet = pUnitsPerPallet;
        setUnitInTotal();
        pushColum(pPalletsPerColum, pUnitsPerPallet);
    }

    /** Push & Pop Methods */
    /** 
     * Creates a Column node and adds it to the linked list.
     * @param pPallets int that represent the pallets per column
     * @param pUnits int that represent the units per pallet.
    */
    void pushColum(int pPallets, int pUnits) {
        for (int index = 0; index < getColums(); index++) {
            // Crafting colum
            Colums *newColum = new Colums(pPallets, pUnits);
            // Pusshing product
            newColum->next = firstColum;
            firstColum = newColum;
        }
    }
    /** 
     * Removes the first column.
    */
    void popColum() {
        Colums *auxColum = firstColum;
        firstColum = auxColum->next;
        delete auxColum;
    }

    /** set & get Methods */
    string getNameProduct() {
        return this->nameProduct;
    }
    void setNameProduct(string pName) {
        this->nameProduct = pName;
    }
    int getColums() {
        return this->columsQuantity;
    }
    void setColums(int pColums) {
        this->columsQuantity = pColums;
    }
    int getPalletsPerColum() {
        return this->palletsPerColumn;
    }
    void setPalletsPerColum(int pPallets) {
        this->palletsPerColumn = pPallets;
    }
    int getUnitsPerPallet() {
        return this->unitsPerPallet;
    }
    void setUnitsPerPallet(int pUnits) {
        this->unitsPerPallet = pUnits;
    }
    int getUnitsInTotal() {
        return this->unitsInTotal;
    }
    void setUnitInTotal() {
        this->unitsInTotal = (getUnitsPerPallet() * getPalletsPerColum()) * (getColums());
    }

    /** 
     * Displays the values of the columns.
    */
    void columsDetails(){    
        if (firstColum != NULL){
            int cont = 0;
            for (Colums *tempColum = firstColum; tempColum != NULL; tempColum = tempColum->next) {
                cout<<"----- Column number " <<++cont <<" -----" <<endl;
                cout<<"Pallets: " <<tempColum->pallets <<endl <<"Units: " <<tempColum->units <<endl;
            }
        }
    }
};

#endif //_PRODUCT_
#include <iostream>
#include <conio.h>
#include "directory/stores.h"
#include "directory/database.h"
#include "directory/elevatorCar.h"
#include "directory/testing.h"
#include "directory/workProcess.h"

#define INDEX 5
#define AVAIABLES 5
#define QUANTITY 5

using namespace std;

int main() {
    /** Declarations */
    Store storesCrisFran[INDEX];
    uploadProducts(storesCrisFran);
    OrderTail orderList[QUANTITY];
    uploadOrders(orderList);
    ElevatorCar pickCars[AVAIABLES];
    asignOrdersToPickCars(orderList, pickCars);

    /** Consulting Data *
    consulting(storesCrisFran, orderList, pickCars);
    
    /** Working Process */
    doWorkProcess(storesCrisFran, pickCars[0]);
    doWorkProcess(storesCrisFran, pickCars[1]);
    doWorkProcess(storesCrisFran, pickCars[2]);
    doWorkProcess(storesCrisFran, pickCars[3]);
    doWorkProcess(storesCrisFran, pickCars[4]);

    /** Consulting Data *
    consulting(storesCrisFran, orderList, pickCars);

    /** System Pause */
    getch();
    return 0;
}